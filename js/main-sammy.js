var app = Sammy('#main', function() {

    this.get('#/', loadHome)
    this.get('#/users', loadUserList);
    this.get('#/experiences', loadExperienceList);
    this.get('#/events',loadEventList);
    this.get('#/contacts',loadInfoContact);
    this.get('#/institutions',loadInstitutions);
    this.get('#/agreements',loadAgreements);

    function loadHome(){
        $( "#main" ).load( "templates/new.html");
    }
    function loadUserList(){
        $( "#main" ).load( "templates/user.html");
    }
    function loadExperienceList(){
        $( "#main" ).load( "templates/experience.html");
    }
    function loadEventList(){
        $( "#main" ).load( "templates/event.html");
    }
    function loadInfoContact(){
        $( "#main" ).load( "templates/contact.html");
    }
    function loadInstitutions(){
        $( "#main" ).load( "templates/institution.html");
    }
    function loadAgreements(){
        $( "#main" ).load( "templates/agreement.html");
    }

});

app.run('#/');