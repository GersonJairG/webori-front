
function deleteEvent(id) {
    EventModule.deleteEvent(id)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function createEvent(nombre, descripcion, lugar, fecha) {    
    EventModule.createEvent(nombre, descripcion,lugar, fecha)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function editEvent(id, nombre, descripcion,lugar, fecha) {
    EventModule.updateEvent(id, nombre, descripcion,lugar, fecha)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function loadEvents() {
    EventModule.getEvents()
        .then(function (response) {
            var options = EventModule.getOptions(response);
            console.log(options);
            $('#event-table').DataTable({
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla.",
                    "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                    "infoPostFix": " ",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Dato para buscar",
                    "zeroRecords": "No se han encontrado coincidencias.",
                    "paginate": {
                        "first": "Primera",
                        "last": "Última",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": "Ordenación ascendente",
                        "sortDescending": "Ordenación descendente"
                    }
                },
                "columnDefs": [
                    { "className": "dt-center", "targets": "_all" }
                ],
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': options,
                'columns': [
                    { title: "#", data: 'id' },
                    { title: "Nombre", data: 'nombre' },
                    { title: "Descripcion", data: 'descripcion' },
                    { title: "Lugar", data: 'lugar' },
                    { title: "Fecha", data: 'fecha' },
                    { title: "Opciones", data: 'options' }
                ],
                'bLengthChange': false
            });

            $('.delete-option').on("click", function () {
                var id = $(this).attr('event-id');
                console.log(id);
                swal({
                    title: "¿Estas seguro de esto?",
                    text: "Si aceptas no podrás recuperar el evento!",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No, cancelar", "Si, eliminar!"]
                }).then((value => {
                    if (value) {
                        deleteEvent(id);
                        swal({
                            title: 'Eliminado!',
                            text: 'El evento ha sido borrado correctamente.',
                            icon: 'success'
                        }).then((value) => {
                            if (value) {
                                refresh();
                            }
                        })
                    }
                })

                )
            });

            $('.edit-option').on('click', function () {
                //Visibilidad de titulos
                $('#title-new-event').css('display', 'none')
                $('#title-edit-event').css('display', 'block');
                //Visibilidad de botones
                $('#new-event-button').css('display', 'none');
                $('#edit-event-button').css('display', 'block');
                //captura de informacion a Variables
                var id = $(this).attr('event-id');
                var nombre = $(this).attr('event-nombre');
                var descripcion = $(this).attr('event-descripcion');
                var lugar = $(this).attr('event-lugar');
                var fecha = $(this).attr('event-fecha');

                $('#id-event').val(id);
                $('#nombre-event').val(nombre);
                $('#descripcion-event').val(descripcion);
                $('#lugar-event').val(lugar);
                $('#fecha-event').val(fecha);

            })

        })
        .catch(function (error) {
            console.log(error);
        });
}
loadEvents();


//Captura de datos luego de dar click en el boton Registrar de un usuario cuando el se registra
$('#new-event-button').on('click', function () {

    var nombre = $('#nombre-event').val();
    var descripcion = $('#descripcion-event').val();
    var lugar = $('#lugar-event').val();
    var fecha = $('#fecha-event').val();
    if (!(nombre == '' || descripcion == '' || fecha == '' || lugar == '')) {
        $(this).attr('data-dismiss', 'modal');
        createEvent(nombre, descripcion, lugar, fecha);
        swal({
            title: 'Evento ' + nombre + ' registrado!',
            text: 'El evento se ha sido creado correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'El evento no ha podido ser creado, complete todos los datos.',
            icon: 'error'
        })
    }

})

//Captura clic de boton Editar para editar el usuario
$('#edit-event-button').on('click', function () {

    var id = $('#id-event').val();
    var nombre = $('#nombre-event').val();
    var descripcion = $('#descripcion-event').val();
    var lugar = $('#lugar-event').val();
    var fecha = $('#fecha-event').val();
    console.log('id: ' + id + ', nombre: ' + nombre + ', descripcion: ' + descripcion + ',lugar:'+ lugar +', fecha :' + fecha);

    if (!(id == '' || nombre == '' || descripcion == '' || fecha == '' || lugar == '' )) {
        $(this).attr('data-dismiss', 'modal');
        editEvent(id, nombre, descripcion, lugar, fecha);
        swal({
            title: 'Cambios registrados del evento ' + name + '!',
            text: 'El evento ha sido editado correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        }).catch(function (error) {
            console.log(error);
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'No se pudo editar la informacion, por favor llene todos los campos solicitados.',
            icon: 'error'
        })
    }

})

//Al oprimir el boton +AÑADIR
$('#open-modal-new-event').on('click', function () {
    //visibilidad de titulos 
    $('#title-new-event').css('display', 'block')
    $('#title-edit-event').css('display', 'none');

    //Visibilidad de botones
    $('#new-event-button').css('display', 'block');
    $('#edit-event-button').css('display', 'none');
    limpiarcampos();
})

function refresh() {
    $('#event-table').dataTable().fnDestroy();
    loadEvents();
}

//metodo que limpia los campos de la intefaz de registro del usuario
function limpiarcampos() {

    $('#nombre-event').val("");
    $('#descripcion-event').val("");
    $('#lugar-event').val("");
    //Setear fecha de hoy
    var now = new Date();
    $('#fecha-event').val(setDate(now));

}

function setDate(now) {
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}
