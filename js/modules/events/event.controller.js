var EventModule = {
    getOptions: function (data) {
        var objects = [];
        data.forEach(function (event) {            
            var fecha = event.fecha.split(' ');           
            var nuevaFecha = fecha[0].split('-');                                        
            event.options = '<button class="edit-option"  data-toggle="modal" data-target="#modal-events" event-id="' + event.id + '" event-descripcion="' + event.descripcion +'" event-lugar="' + event.lugar +  '" event-fecha="' + fecha[0] +'" event-nombre="' + event.nombre + '" data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></button><button class="delete-option" event-id="' + event.id + '" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            event.fecha = nuevaFecha[2]+'/'+nuevaFecha[1]+'/'+nuevaFecha[0];          
            objects.push(event);
        });
        return objects;
    },
    getEvents: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/eventos',
            data: {

            }
        });
    },
    createEvent: function (nombre, descripcion, lugar, fecha) {        
        return $.ajax({
            method: 'POST',
            url: constants.URL_API + '/eventos/new',
            data: {
                nombre: nombre,
                descripcion: descripcion,
                fecha: fecha,
                lugar: lugar
            }
        })
    },
    deleteEvent: function (id) {
        return $.ajax({
            method: 'delete',
            url: constants.URL_API + '/eventos/' + id,
            data: {
                id: id
            }
        })
    },
    updateEvent: function (id, nombre, descripcion,lugar, fecha) {
        return $.ajax({
            method: 'patch',
            url: constants.URL_API + '/eventos/' + id,
            data: {
                id: id,
                nombre: nombre,
                fecha: fecha,
                descripcion: descripcion,
                lugar : lugar
            }
        })
    }


}