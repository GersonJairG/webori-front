
var InstitutionModule = {
    getOptions: function (data) {
        var objects = [];    
        data.forEach(function(institution) {
            institution.options = '<button class="edit-option"  data-toggle="modal" data-target="#modal-institutions" institution-id="'+institution.id+'" institution-nombre="'+institution.nombre+'" institution-pais="'+institution.id_pais+'" institution-tipo="'+institution.id_tipo_institucion+'" data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></button><button class="delete-option" institution-id="'+institution.id+'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            console.log('CONTROLLER: ')
            console.log(institution);
            objects.push(institution);
        });                
        return objects;
    },
    getInstitutions: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/instituciones',
            data: {

            }
        });
    },
    getCountry: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/paises',
            data: {

            }
        });
    },
    createInstitution: function (nombre, idPais, idTipoInstitucion) {
        return $.ajax({
            method: 'POST',
            url: constants.URL_API + '/instituciones/new',
            data: {                
                nombre: nombre,
                idPais : idPais,
                idTipoInstitucion : idTipoInstitucion                
            }
        })
    },
    deleteInstitution: function (id) {
        return $.ajax({
            method: 'delete',
            url: constants.URL_API + '/instituciones/' + id,
            data: {
                id: id
            }
        })
    },
    updateInstitution: function(id,nombre, idPais, idTipoInstitucion){        
        return $.ajax({
            method : 'patch',
            url : constants.URL_API + '/instituciones/'+id,
            data : {
                id: id,
                inombre: nombre,
                idPais : idPais,
                idTipoInstitucion : idTipoInstitucion   
            }
        })
    }

}