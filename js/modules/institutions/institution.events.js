
function deleteInstitution(id) {
    InstitutionModule.deleteInstitution(id)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function createInstitution(nombre, idPais, idTipoInstitution) {    
    InstitutionModule.createInstitution(nombre, idPais, idTipoInstitution)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function editInstitution(id, nombre, idPais, idTipoInstitution) {
    InstitutionModule.updateInstitution(id , nombre, idPais, idTipoInstitution)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}
function loadContries(){
    InstitutionModule.getCountry()
    .then(response => {    
        console.log(response);    
        $("#pais-institucion").html("<option value=0>Seleccionar Pais</option>");    
        response.forEach(country => {
            $("#pais-institucion").append(
                "<option value="+ country.id +">"+country.nombre +"</option>"
            );
        });
    })
    .catch(error => {        
        console.log(error);
    })
}


function loadInstitutions() {
    InstitutionModule.getInstitutions()
        .then(function (response) {
            var options = InstitutionModule.getOptions(response);
            console.log(options);
            $('#institution-table').DataTable({
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla.",
                    "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                    "infoPostFix": " ",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Dato para buscar",
                    "zeroRecords": "No se han encontrado coincidencias.",
                    "paginate": {
                        "first": "Primera",
                        "last": "Última",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": "Ordenación ascendente",
                        "sortDescending": "Ordenación descendente"
                    }
                },
                "columnDefs": [
                    { "className": "dt-center", "targets": "_all" }
                ],
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': options,
                'columns': [
                    { title: "#", data: 'id' },
                    { title: "Nombre", data: 'nombre' },
                    { title: "Pais", data: 'pais' },
                    { title: "Tipo", data: 'tipo' },                    
                    { title: "Opciones", data: 'options' }
                ],
                'bLengthChange': false
            });

            $('.delete-option').on("click", function () {
                var id = $(this).attr('institution-id');
                console.log(id);
                swal({
                    title: "¿Estas seguro de esto?",
                    text: "Si aceptas no podrás recuperar la institucion!",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No, cancelar", "Si, eliminar!"]
                }).then((value => {
                    if (value) {
                        deleteInstitution(id);
                        swal({
                            title: 'Eliminado!',
                            text: 'La institucion ha sido borrado correctamente.',
                            icon: 'success'
                        }).then((value) => {
                            if (value) {
                                refresh();
                            }
                        })
                    }
                })

                )
            });

            $('.edit-option').on('click', function () {
                //Visibilidad de titulos
                $('#title-new-institution').css('display', 'none')
                $('#title-edit-institution').css('display', 'block');
                //Visibilidad de botones
                $('#new-institution-button').css('display', 'none');
                $('#edit-institution-button').css('display', 'block');
                //captura de informacion a Variables
                var id = $(this).attr('institution-id');
                var nombre = $(this).attr('institution-nombre');
                var pais = $(this).attr('institution-pais');
                console.log(pais);
                var tipo = $(this).attr('institution-tipo');                
                
                $('#id-institucion').val(id);
                $('#nombre-institucion').val(nombre);
                $('#pais-institucion').val(pais);
                $('#tipo-institucion').val(tipo);            

            })

        })
        .catch(function (error) {
            console.log(error);
        });
}

loadInstitutions();
loadContries();

//Captura de datos luego de dar click en el boton Registrar de un usuario cuando el se registra
$('#new-institution-button').on('click', function () {

    var nombre = $('#nombre-institucion').val();
    var pais = $('#pais-institucion').val();
    var tipo = $('#tipo-institucion').val();    

    if (!(nombre == '' || pais == 0 || tipo == 0)) {
        $(this).attr('data-dismiss', 'modal');
        createInstitution(nombre,pais,tipo);        
        swal({
            title: 'Institucion ' + nombre + ' registrada!',
            text: 'La institucion ha sido creado correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'La institucion no ha podido ser creada, complete todos los datos.',
            icon: 'error'
        })
    }

})

// //Captura clic de boton Editar para editar el usuario
$('#edit-institution-button').on('click', function () {

    var id = $('#id-institucion').val();
    var nombre = $('#nombre-institucion').val();
    var pais = $('#pais-institucion').val();
    var tipo = $('#tipo-institucion').val();        

    if (!(id == '' || nombre == '' || pais == 0 || tipo == 0)) {
        $(this).attr('data-dismiss', 'modal');
        editInstitution(id, nombre, pais, tipo);
        swal({
            title: 'Cambios registrados de la institucion ' + nombre + '!',
            text: 'La institucion ha sido editada correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        }).catch(function (error) {
            console.log(error);
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'No se pudo editar la institucion, por favor llene todos los campos solicitados.',
            icon: 'error'
        })
    }

})

//Al oprimir el boton +AÑADIR
$('#open-modal-new-institution').on('click', function () {
    
    //visibilidad de titulos 
    $('#title-new-institution').css('display', 'block')
    $('#title-edit-institution').css('display', 'none');

    //Visibilidad de botones
    $('#new-institution-button').css('display', 'block');
    $('#edit-institution-button').css('display', 'none');
    limpiarcampos();
})

function refresh() {
    $('#institution-table').dataTable().fnDestroy();
    loadInstitutions();
}

// //metodo que limpia los campos de la intefaz de registro del usuario
function limpiarcampos() {

    $('#id-institucion').val("");
    $('#nombre-institucion').val("");
    $('#pais-institucion').val(0);
    $('#tipo-institucion').val(0);  
}
