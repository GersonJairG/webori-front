function deleteExperience(id) {
    ExperienceModule.deleteExperience(id)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function createExperience(titulo, descripcion) {
    ExperienceModule.createExperience(titulo, descripcion)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function editExperience(id, titulo, descripcion) {
    ExperienceModule.updateExperience(id, titulo, descripcion)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function loadExperiences() {
    ExperienceModule.getExperiences()
        .then(function (response) {
            var options = ExperienceModule.getOptions(response);
            console.log(options);
            $('#experience-table').DataTable({
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla.",
                    "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                    "infoPostFix": " ",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Dato para buscar",
                    "zeroRecords": "No se han encontrado coincidencias.",
                    "paginate": {
                        "first": "Primera",
                        "last": "Última",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": "Ordenación ascendente",
                        "sortDescending": "Ordenación descendente"
                    }
                },
                "columnDefs": [
                    { "className": "dt-center", "targets": "_all" }
                ],
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': options,
                'columns': [
                    { title: "#", data: 'id' },
                    { title: "Titulo", data: 'titulo' },
                    { title: "Descripcion", data: 'descripcion' },
                    { title: "Opciones", data: 'options' }
                ],
                'bLengthChange': false
            });

            $('.delete-option').on("click", function () {
                var id = $(this).attr('experience-id');
                console.log(id);
                swal({
                    title: "¿Estas seguro de esto?",
                    text: "Si aceptas no podrás recuperar la experiencia!",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No, cancelar", "Si, eliminar!"]
                }).then((value => {
                    if (value) {
                        deleteExperience(id);
                        swal({
                            title: 'Eliminado!',
                            text: 'La experiencia ha sido borrada correctamente.',
                            icon: 'success'
                        }).then((value) => {
                            if (value) {
                                refresh();
                            }
                        })
                    }
                })

                )
            });

            $('.edit-option').on('click', function () {
                //Visibilidad de titulos
                $('#title-new-experience').css('display', 'none')
                $('#title-edit-experience').css('display', 'block');
                //Visibilidad de botones
                $('#new-experience-button').css('display', 'none');
                $('#edit-experience-button').css('display', 'block');
                //captura de informacion a Variables
                var id = $(this).attr('experience-id');
                var titulo = $(this).attr('experience-titulo');
                var descripcion = $(this).attr('experience-descripcion');

                $('#id-experience').val(id);
                $('#titulo-experience').val(titulo);
                $('#descripcion-experience').val(descripcion);

            })

        })
        .catch(function (error) {
            // TODO: Manejar mensaje de error
        });
}
loadExperiences();


//Captura de datos luego de dar click en el boton Registrar de un usuario cuando el se registra
$('#new-experience-button').on('click', function () {

    var titulo = $('#titulo-experience').val();
    var descripcion = $('#descripcion-experience').val();

    if (!(titulo == '' || descripcion == '')) {
        $(this).attr('data-dismiss', 'modal');
        createExperience(titulo, descripcion);
        swal({
            title: 'Experiencia ' + titulo + ' registrada!',
            text: 'La experiencia se ha sido creado correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'La experiencia no ha podido ser creado, complete todos los datos.',
            icon: 'error'
        })
    }

})

//---------------------------VOY POR ACAAA------------------------------------------------
//Captura clic de boton Editar para editar el usuario
$('#edit-experience-button').on('click', function () {

    var id = $('#id-experience').val();
    var titulo = $('#titulo-experience').val();
    var descripcion = $('#descripcion-experience').val();    

    console.log('id: ' + id + ', titulo: ' + titulo + ', descripcion: ' + descripcion);

    if (!(id == '' || titulo == '' || descripcion == '')) {        
            $(this).attr('data-dismiss', 'modal');            
            editExperience(id, titulo, descripcion);
            swal({
                title: 'Cambios registrados de la experiencia ' + name + '!',
                text: 'La experiencia ha sido editado correctamente.',
                icon: 'success'
            }).then((value) => {
                if (value) {
                    limpiarcampos();
                    refresh();
                }
            }).catch(function (error) {
                console.log(error);
            })        
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'No se pudo editar la informacion, por favor llene todos los campos solicitados.',
            icon: 'error'
        })
    }

})

//Al oprimir el boton +AÑADIR
$('#open-modal-new-experience').on('click', function () {
    //visibilidad de titulos 
    $('#title-new-experience').css('display', 'block')
    $('#title-edit-experience').css('display', 'none');

    //Visibilidad de botones
    $('#new-experience-button').css('display', 'block');
    $('#edit-experience-button').css('display', 'none');
    limpiarcampos();
})

function refresh() {
    $('#experience-table').dataTable().fnDestroy();
    loadExperiences();
}

//metodo que limpia los campos de la intefaz de registro del usuario
function limpiarcampos() {
    $('#titulo-experience').val("");
    $('#descripcion-experience').val("");

}
