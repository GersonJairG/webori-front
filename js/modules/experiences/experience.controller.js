var ExperienceModule = {
    getOptions: function (data) {
        var objects = [];    
        data.forEach(function(experience) {
            experience.options = '<button class="edit-option"  data-toggle="modal" data-target="#modal-experiences" experience-id="'+experience.id+'" experience-descripcion="'+experience.descripcion+'" experience-titulo="'+experience.titulo+'" data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></button><button class="delete-option" experience-id="'+experience.id+'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            objects.push(experience);
        });                
        return objects;
    },
    getExperiences: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/experiencias',
            data: {

            }
        });
    },
    createExperience: function (titulo, descripcion) {
        return $.ajax({
            method: 'POST',
            url: constants.URL_API + '/experiencias/new',
            data: {                
                titulo: titulo,
                descripcion: descripcion
            }
        })
    },
    deleteExperience: function (id) {
        return $.ajax({
            method: 'delete',
            url: constants.URL_API + '/experiencias/' + id,
            data: {
                id: id
            }
        })
    },
    updateExperience: function(id, titulo, descripcion){
        return $.ajax({
            method : 'patch',
            url : constants.URL_API + '/experiencias/'+id,
            data : {
                id : id,
                titulo : titulo,
                descripcion: descripcion
            }
        })
    }

}