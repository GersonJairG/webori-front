
function deleteAgreement(id) {
    AgreementModule.deleteAgreement(id)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function createAgreement(año, idTipoConvenio, idInstitucion) {    
    AgreementModule.createAgreement(año, idTipoConvenio, idInstitucion)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function editAgreement(id, año, idTipoConvenio, idInstitucion) {
    AgreementModule.updateAgreement(id , año, idTipoConvenio, idInstitucion)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function loadInstitutions(){
    AgreementModule.getInstitution()
    .then(response => {    
        console.log(response);    
        $("#institucion-convenio").html("<option value=0>Seleccionar Institucion</option>");    
        response.forEach(institution => {
            $("#institucion-convenio").append(
                "<option value="+ institution.id +">"+institution.nombre +"</option>"
            );
        });
    })
    .catch(error => {        
        console.log(error);
    })
}


function loadAgreements() {
    AgreementModule.getAgreements()
        .then(function (response) {
            var options = AgreementModule.getOptions(response);
            console.log(options);
            $('#agreement-table').DataTable({
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla.",
                    "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                    "infoPostFix": " ",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Dato para buscar",
                    "zeroRecords": "No se han encontrado coincidencias.",
                    "paginate": {
                        "first": "Primera",
                        "last": "Última",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": "Ordenación ascendente",
                        "sortDescending": "Ordenación descendente"
                    }
                },
                "columnDefs": [
                    { "className": "dt-center", "targets": "_all" }
                ],
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': options,
                'columns': [
                    { title: "#", data: 'id' },
                    { title: "Año", data: 'año' },
                    { title: "tipo", data: 'tipo_convenio' },
                    { title: "Institucion", data: 'institucion' },                    
                    { title: "Opciones", data: 'options' }
                ],
                'bLengthChange': false
            });

            $('.delete-option').on("click", function () {
                var id = $(this).attr('agreement-id');
                console.log(id);
                swal({
                    title: "¿Estas seguro de esto?",
                    text: "Si aceptas no podrás recuperar el convenio!",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No, cancelar", "Si, eliminar!"]
                }).then((value => {
                    if (value) {                        
                        deleteAgreement(id)
                        swal({
                            title: 'Eliminado!',
                            text: 'El convenio ha sido borrado correctamente.',
                            icon: 'success'
                        }).then((value) => {
                            if (value) {
                                refresh();
                            }
                        })
                    }
                })

                )
            });

            $('.edit-option').on('click', function () {
                //Visibilidad de titulos
                $('#title-new-agreement').css('display', 'none')
                $('#title-edit-agreement').css('display', 'block');
                //Visibilidad de botones
                $('#new-agreement-button').css('display', 'none');
                $('#edit-agreement-button').css('display', 'block');
                //captura de informacion a Variables
                var id = $(this).attr('agreement-id');
                var año = $(this).attr('agreement-año');
                var tipoConvenio = $(this).attr('agreement-tipo');                
                var institucion = $(this).attr('agreement-institucion');                
                
                $('#id-convenio').val(id);
                $('#año-convenio').val(año);
                $('#tipo-convenio').val(tipoConvenio);
                $('#institucion-convenio').val(institucion);            

            })

        })
        .catch(function (error) {
            console.log(error);
        });
}

loadAgreements();
loadInstitutions();

// //Captura de datos luego de dar click en el boton Registrar de un usuario cuando el se registra
$('#new-agreement-button').on('click', function () {

    var año = $('#año-convenio').val();
    var tipoConvenio = $('#tipo-convenio').val();
    var institucion = $('#institucion-convenio').val();    

    if (!(año == 0 || tipoConvenio == 0 || institucion == 0)) {
        $(this).attr('data-dismiss', 'modal');
        createAgreement(año,tipoConvenio,institucion)    
        swal({
            title: 'Nuevo convenio registrado!',
            text: 'El convenio ha sido creado correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'El convenio no ha podido ser creado, complete todos los datos.',
            icon: 'error'
        })
    }

})

// // //Captura clic de boton Editar para editar el usuario
$('#edit-agreement-button').on('click', function () {

    var id = $('#id-convenio').val();
    var año = $('#año-convenio').val();
    var tipoConvenio = $('#tipo-convenio').val();
    var institucion = $('#institucion-convenio').val();        

    if (!(id == '' || año == 0 || tipoConvenio == 0 || institucion == 0)) {
        $(this).attr('data-dismiss', 'modal');
        editAgreement(id, año, tipoConvenio, institucion)
        swal({
            title: 'Cambios registrados del convenio #' + id + '!',
            text: 'El convenio ha sido editada correctamente.',
            icon: 'success'
        }).then((value) => {
            if (value) {
                limpiarcampos();
                refresh();
            }
        }).catch(function (error) {
            console.log(error);
        })
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'No se pudo editar el convenio, por favor llene todos los campos solicitados.',
            icon: 'error'
        })
    }

})

//Al oprimir el boton +AÑADIR
$('#open-modal-new-agreement').on('click', function () {
    
    //visibilidad de titulos 
    $('#title-new-agreement').css('display', 'block')
    $('#title-edit-agreement').css('display', 'none');

    //Visibilidad de botones
    $('#new-agreement-agreement').css('display', 'block');
    $('#edit-agreement-button').css('display', 'none');
    limpiarcampos();
})

function refresh() {
    $('#agreement-table').dataTable().fnDestroy();
    loadAgreements();
}

// // //metodo que limpia los campos de la intefaz de registro del usuario
function limpiarcampos() {

    $('#año-convenio').val(0);
    $('#tipo-convenio').val(0);
    $('#institucion-convenio').val(0);
}
