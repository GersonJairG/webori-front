$('#correo-login').focus(function(){
    $('#error-correo').css('display','none');
    $('#error-field').css('display','none'); 
    $('#error-access').css('display','none'); 
});

$('#contrasena-login').focus(function(){
    $('#error-contrasena').css('display','none');
    $('#error-field').css('display','none'); 
    $('#error-access').css('display','none'); 
});

function verifyLogin() {

    var password = $('#contrasena-login').val();
    var email = $('#correo-login').val();

    loginModule.sessionClear();

    //Verificacion email
    if (email != '') {
        if (password != '') {
            if (helper.checkEmail(email)) {
                loginModule.login(email, password)
                    .then(function (response) {                        
                        console.log(response);
                        if (response != '') {
                            loginModule.sessionCreate({
                                id: response.id,
                                nombre: response.nombre,
                                documento: response.documento,
                                sexo: response.sexo,
                                rol: response.rol,
                                email: response.email
                            });                                                                                                                           
                            location.href = constants.URL_VIEW+'/dashboard.html';                        
                        } else {
                            $('#error-access').css('display','block');
                        }

                    })
                    .catch(function (error) {
                        console.log('entro al catch');
                        console.log(error);
                    });
            } else {
                $('#error-correo').css('display','block');
            }
        } else {
            $('#error-field').css('display','block');
        }
    } else {
        $('#error-field').css('display','block');
    }


}

$('#login').on('click',verifyLogin);

$('#logout').on('click', function () {    
    loginModule.sessionClear();
})