var loginModule = {
    login: function(correo, contrasena) {
        return $.ajax({
            method : 'POST',
            url : constants.URL_API + '/login',
            data: {
                correo: correo,
                contrasena: contrasena
            }
        });
    },
    resetPassword: function() {
    },
    sessionClear: function () {
        store.clearAll();
    },
    sessionCreate: function (userInfo) {
        store.set('user', { id: userInfo.id, sexo: userInfo.sexo, nombre: userInfo.nombre, email: userInfo.email, rol: userInfo.rol, documento: userInfo.documento });
    }
}