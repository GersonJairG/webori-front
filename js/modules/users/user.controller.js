
var UserModule = {
    getOptions: function (data) {
        var objects = [];    
        data.forEach(function(user) {
            user.options = '<button class="edit-option"  data-toggle="modal" data-target="#modal-users" user-id="'+user.id+'" user-documento="'+user.documento+'" user-nombre="'+user.nombre+'" user-correo="'+user.email+'" user-rol="'+user.rol+'" user-sexo="'+user.sexo+'" data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></button><button class="delete-option" user-id="'+user.id+'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            objects.push(user);
        });                
        return objects;
    },
    getUsers: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/usuarios',
            data: {

            }
        });
    },
    createUser: function (id, nombre, documento, correo, contrasena, sexo, idRol) {
        return $.ajax({
            method: 'POST',
            url: constants.URL_API + '/usuarios/new',
            data: {
                id: id,
                nombre: nombre,
                documento: documento,
                correo: correo,
                contrasena: contrasena,
                sexo: sexo,
                idRol: idRol
            }
        })
    },
    deleteUser: function (id) {
        return $.ajax({
            method: 'delete',
            url: constants.URL_API + '/usuarios/' + id,
            data: {
                id: id
            }
        })
    },
    updateUser: function(id, nombre, documento, correo, sexo, idRol){
        console.log('En updateUser:' + id +','+nombre+','+documento+','+correo+','+sexo+','+idRol )
        return $.ajax({
            method : 'patch',
            url : constants.URL_API + '/usuarios/'+id,
            data : {
                id : id,
                documento: documento,
                nombre : nombre,                
                correo: correo,                
                sexo : sexo,
                idRol : idRol
            }
        })
    }

}