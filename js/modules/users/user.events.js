function deleteUser(id) {
    UserModule.deleteUser(id)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function createUser(id, nombre, documento, correo, contrasena, sexo, idRol) {
    UserModule.createUser(id, nombre, documento, correo, contrasena, sexo, idRol)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function editUser(id, nombre, documento, correo, sexo, idRol) {
    UserModule.updateUser(id, nombre, documento, correo, sexo, idRol)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function loadUsers() {
    UserModule.getUsers()
        .then(function (response) {
            console.log(response);
            var options = UserModule.getOptions(response);
            console.log(options);
            $('#user-table').DataTable({
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla.",
                    "info": "Registros del _START_ al _END_ de _TOTAL_ ",
                    "infoEmpty": "Mostrando 0 registros de un total de 0.",
                    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                    "infoPostFix": " ",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Dato para buscar",
                    "zeroRecords": "No se han encontrado coincidencias.",
                    "paginate": {
                        "first": "Primera",
                        "last": "Última",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": "Ordenación ascendente",
                        "sortDescending": "Ordenación descendente"
                    }
                },
                "columnDefs": [
                    { "className": "dt-center", "targets": "_all" }
                ],
                "autoWidth": false,
                'retrieve': true,//permite volver a cargar la tabla con mas valores o los mismos
                'data': options,
                'columns': [
                    { title: "Codigo", data: 'id' },
                    { title: "Documento", data: 'documento' },
                    { title: "Nombre", data: 'nombre' },
                    { title: "Correo", data: 'email' },
                    { title: "Rol", data: 'rol' },
                    { title: "Sexo", data: 'sexo' },
                    { title: "Opciones", data: 'options' }
                ],
                'bLengthChange': false
            });

            $('.delete-option').on("click", function () {
                var id = $(this).attr('user-id');
                console.log(id);
                swal({
                    title: "¿Estas seguro de esto?",
                    text: "Si aceptas no podrás recuperar el usuario!",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No, cancelar", "Si, eliminar!"]
                }).then((value => {
                    if (value) {
                        deleteUser(id);
                        swal({
                            title: 'Eliminado!',
                            text: 'El usuario ha sido borrado correctamente.',
                            icon: 'success'
                        }).then((value) => {
                            if (value) {
                                refresh();
                            }
                        })
                    }
                })

                )
            });

            $('.edit-option').on('click', function () {
                //Visibilidad de titulos
                $('#title-new-user').css('display', 'none')
                $('#title-edit-user').css('display', 'block');
                //Visibilidad de botones
                $('#new-user-button').css('display', 'none');
                $('#edit-user-button').css('display', 'block');
                //visibilidad campo contraseña
                $('#contrasena-title').css('display', 'none');
                $('#contrasena-user').css('display', 'none');
                //captura de informacion a Variables
                var id = $(this).attr('user-id');
                var documento = $(this).attr('user-documento');
                var nombre = $(this).attr('user-nombre');
                var correo = $(this).attr('user-correo');
                var idRol = $(this).attr('user-rol');
                var sexo = $(this).attr('user-sexo');

                console.log('AL ABRIR EDITAR, idRol: ' + idRol + ', sexo:' + sexo);

                $('#id-user').val(id);
                $('#documento-user').val(documento);
                $('#nombre-user').val(nombre);
                $('#correo-user').val(correo);
                $('#sexo-user').val(sexo);
                //TODO: Esto no se debe hacer, pero por motivos de tiempo se hara :) =  
                if (idRol == 'Estudiante') {
                    $('#rol-user').val(1);
                } else if (idRol == 'Docente') {
                    $('#rol-user').val(2);
                } else if (idRol == 'Docente') {
                    $('#rol-user').val(3);
                } else {
                    console.log('Funcion de buscar el id del rol por su nombre');
                }

            })

        })
        .catch(function (error) {
            // TODO: Manejar mensaje de error
        });
}
loadUsers();


//Captura de datos luego de dar click en el boton Registrar de un usuario cuando el se registra
$('#new-user-button').on('click', function () {

    var id = $('#id-user').val();
    var nombre = $('#nombre-user').val();
    var documento = $('#documento-user').val();
    var correo = $('#correo-user').val();
    var contrasena = $('#contrasena-user').val();
    var sexo = $('#sexo-user').val();
    var idRol = $('#rol-user').val();

    console.log('id: ' + id + ', nombre: ' + nombre + ', documento: ' + documento + ', correo: ' + correo + ', contraseña: ' + contrasena + ', sexo: ' + sexo + ', idRol: ' + idRol + '');
    if (!(id == '' || nombre == '' || documento == '' || correo == '' || contrasena == '' || sexo == '' || idRol == '')) {
        if (!helper.checkOnlyNumbers(id)) {
            swal({
                title: 'Error!',
                text: 'El campo codigo solo acepta caracteres numericos.',
                icon: 'error'
            })
            $(this).attr('data-dismiss', '');
        } else if (!helper.checkOnlyNumbers(documento)) {
            $(this).attr('data-dismiss', '');
            swal({
                title: 'Error!',
                text: 'El campo documento solo acepta caracteres numericos.',
                icon: 'error'
            })
        } else if (helper.checkEmail(correo)) {
            $(this).attr('data-dismiss', 'modal');
            createUser(id, nombre, documento, correo, contrasena, sexo, idRol);
            swal({
                title: 'Usuario ' + nombre + ' Registrado!',
                text: 'El usuario ha sido creado correctamente.',
                icon: 'success'
            }).then((value) => {
                if (value) {
                    limpiarcampos();
                    refresh();
                }
            })
        } else {
            swal({
                title: 'Error!',
                text: 'El usuario no ha podido ser creado, ingrese un correo valido.',
                icon: 'error'
            })
        }
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'El usuario no ha podido ser creado, complete todos los datos.',
            icon: 'error'
        })
    }

})


//Captura clic de boton Editar para editar el usuario
$('#edit-user-button').on('click', function () {

    var id = $('#id-user').val();
    var nombre = $('#nombre-user').val();
    var documento = $('#documento-user').val();
    var correo = $('#correo-user').val();
    var sexo = $('#sexo-user').val();
    var idRol = $('#rol-user').val();

    console.log('id: ' + id + ', nombre: ' + nombre + ', documento: ' + documento + ', correo: ' + correo + ', sexo: ' + sexo + ', idRol: ' + idRol + '');

    if (!(id == '' || nombre == '' || documento == '' || correo == '' || sexo == '' || idRol == '')) {
        if (!helper.checkOnlyNumbers(id)) {
            swal({
                title: 'Error!',
                text: 'El campo codigo solo acepta caracteres numericos.',
                icon: 'error'
            })
            $(this).attr('data-dismiss', '');
        } else if (!helper.checkOnlyNumbers(documento)) {
            $(this).attr('data-dismiss', '');
            swal({
                title: 'Error!',
                text: 'El campo documento solo acepta caracteres numericos.',
                icon: 'error'
            })
        } else if (helper.checkEmail(correo)) {
            $(this).attr('data-dismiss', 'modal');
            console.log('variables de Edituser:' + id + ',' + nombre + ',' + documento + ',' + correo + ',' + sexo + ',' + idRol);
            editUser(id, nombre, documento, correo, sexo, idRol);
            swal({
                title: 'Cambios registrados del usuario ' + nombre + '!',
                text: 'El usuario ha sido editado correctamente.',
                icon: 'success'
            }).then((value) => {
                if (value) {
                    limpiarcampos();
                    refresh();
                }
            }).catch(function (error) {
                    console.log(error);
            })
        } else {
            swal({
                title: 'Error!',
                text: 'El usuario no ha podido ser editado, ingrese un correo valido.',
                icon: 'error'
            })
        }
    } else {
        $(this).attr('data-dismiss', '');
        swal({
            title: 'Datos incompletos!',
            text: 'No se pudo editar la informacion, por favor llene todos los campos solicitados.',
            icon: 'error'
        })
    }

})

//Al oprimir el boton +AÑADIR
$('#open-modal-new-user').on('click', function () {
    //visibilidad de titulos 
    $('#title-new-user').css('display', 'block')
    $('#title-edit-user').css('display', 'none');

    //Visibilidad de botones
    $('#new-user-button').css('display', 'block');
    $('#edit-user-button').css('display', 'none');

    //visibilidad titulo y campo contraseña
    $('#contrasena-title').css('display', 'block');
    $('#contrasena-user').css('display', 'block');
    //Se limpian los campos por que son los mismos usados al editar.
    limpiarcampos();
})

function refresh() {
    $('#user-table').dataTable().fnDestroy();
    loadUsers();
}

//metodo que limpia los campos de la intefaz de registro del usuario
function limpiarcampos() {
    $('#id-user').val("");
    $('#nombre-user').val("");
    $('#documento-user').val("");
    $('#correo-user').val("");
    $('#contrasena-user').val("");
    $('#rol-user').val("1");
    $('#sexo-user').val("Masculino");
}
