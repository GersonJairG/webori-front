var ContactModule = {
    getContact: function () {
        return $.ajax({
            method: 'get',
            url: constants.URL_API + '/contactos',
            data: {

            }
        });
    },
    updateContact: function (id, ubicacionU, ubicacionOri, email, telefono, facebook, google, twitter, linkedIn, instagram, youtube) {
        return $.ajax({
            method: 'patch',
            url: constants.URL_API + '/contactos/' + id,
            data: {
                id: id,
                ubicacionU: ubicacionU,
                ubicacionOri: ubicacionOri,
                email: email,
                telefono: telefono,
                facebook: facebook,
                google: google,
                twitter: twitter,
                linkedIn : linkedIn,
                instagram: instagram,
                youtube : youtube
            }
        })
    }


}