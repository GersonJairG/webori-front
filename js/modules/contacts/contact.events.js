//Falta terminar de cargar los datos en los campos
//y hacer el editar.

function editContact(id, ubicacionU, ubicacionOri, email, telefono, facebook, google, twitter, linkedIn, instagram, youtube) {
    ContactModule.updateContact(id, ubicacionU, ubicacionOri, email, telefono, facebook, google, twitter, linkedIn, instagram, youtube)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        })
}

function loadInfo() {
    ContactModule.getContact()
        .then(function (response) {
            var infoContact = response[0];            
            $('#ubicacion-u-contact').val(infoContact.ubicacion_u);
            $('#ubicacion-ori-contact').val(infoContact.ubicacion_ori);
            $('#email-contact').val(infoContact.email);
            $('#telefono-contact').val(infoContact.telefono);
            $('#facebook-contact').val(infoContact.facebook);
            $('#google-contact').val(infoContact.google);
            $('#twitter-contact').val(infoContact.twitter);
            $('#linkedIn-contact').val(infoContact.linkedIn);
            $('#instagram-contact').val(infoContact.instagram);
            $('#youtube-contact').val(infoContact.youtube);            
        })
        .catch(function (error) {
            // TODO: Manejar mensaje de error
        });
}

$('#edit-contact').on('click', function(){
    var id = 1;
    var ubicacionU = $('#ubicacion-u-contact').val();
    var ubicacionOri = $('#ubicacion-ori-contact').val();
    var email = $('#email-contact').val();
    var telefono = $('#telefono-contact').val();
    var facebook = $('#facebook-contact').val();
    var google = $('#google-contact').val();
    var twitter = $('#twitter-contact').val();
    var linkedIn = $('#linkedIn-contact').val();
    var instagram = $('#instagram-contact').val();
    var youtube = $('#youtube-contact').val();

    editContact(id, ubicacionU,ubicacionOri,email,telefono,facebook,google,twitter,linkedIn,instagram,youtube);
            swal({
                title: 'Cambios registrado en la informacion de contacto!',
                text: 'El contacto ha sido editado correctamente.',
                icon: 'success'
            })
})

loadInfo();

