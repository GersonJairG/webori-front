$('#logo-dashboard').on('click', function(){
    loadCounts();
})

function loadCounts() {
    UserModule.getUsers()
        .then(function (response) {
            console.log(response);
            var i = 0;
            response.forEach(user => {
                console.log(user.nombre);
                i = i + 1;
            });
            $('#count-users').text(i);
        })
        .catch(function (error) {
            console.log(error);
        })

    ExperienceModule.getExperiences()
        .then(function (response) {
            console.log(response);
            var i = 0;
            response.forEach(experience => {
                console.log(experience.titulo);
                i = i + 1;
            });
            $('#count-experiencies').text(i);
        })
        .catch(function (error) {
            console.log(error);
        })

    EventModule.getEvents()
        .then(function (response) {
            console.log(response);
            var i = 0;
            response.forEach(event => {
                console.log(event.nombre);
                i = i + 1;
            });
            $('#count-events').text(i);
        })
        .catch(function (error) {
            console.log(error);
        })
//
    AgreementModule.getAgreements()
        .then(function (response) {
            console.log(response);
            var i = 0;
            response.forEach(agreement => {
                console.log(agreement.id);
                i = i + 1;
            });
            $('#count-agreements').text(i);
        })
        .catch(function (error) {
            console.log(error);
        })
}

loadCounts();